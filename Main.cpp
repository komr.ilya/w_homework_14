#include <iostream>
#include <string>

int main()
{
	std::string name, age;

	std::cout << "Enter your name: ";
	std::getline(std::cin, name);

	std::cout << "Enter your age: ";
	std::getline(std::cin, age);

	std::cout << "\nname: " << name << "\nage: " << age;
}